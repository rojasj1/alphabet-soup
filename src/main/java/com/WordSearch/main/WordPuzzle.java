package com.WordSearch.main;
import java.util.*;
import java.util.Scanner;
import java.io.*;
import java.io.File;

public class WordPuzzle {

	public static void main(String[] args) throws FileNotFoundException  
	{
		// TODO Auto-generated method stub
		
		String file= "src/main/resources/input.txt";
        Scanner sc = new Scanner(new File(file));
        
        //parsing size of grid
        String boardSize=sc.nextLine();
        int rowSize= Integer.parseInt(boardSize.split("x")[0]);
        int colSize= Integer.parseInt(boardSize.split("x")[1]);

        //construct the board
        String[][] board = new String[rowSize][colSize];

        for(int i=0; i < rowSize; i++)
        {
            board[i]=sc.nextLine().split(" ");
        }
        
      //parse for word bank
        List<String> wordBank = new ArrayList<String>();

        while(sc.hasNextLine())
        {
            wordBank.add(sc.nextLine());
        }
        
        Crossword crossword = new Crossword(board, rowSize, colSize);
        for(String word: wordBank)
        {
            crossword.wordSearch(word); 
        } 
	}

}
